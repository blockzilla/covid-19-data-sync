covid-19-data-sync
----------

Load COVID-19 daily case data from [COVID-19](https://github.com/CSSEGISandData/COVID-19) and push to Google Storage Bucket


# Config
Set Google Service Account credentials

```
export GOOGLE_APPLICATION_CREDENTIALS="/home/user/Downloads/[FILE_NAME].json"
```


# Run App

```
make dev
```


# Docker container

```
make docker-build
make docker-build
make docker-push
```
