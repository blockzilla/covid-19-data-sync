CREATE TABLE IF NOT EXISTS cases (
    id SERIAL PRIMARY KEY,
    town text,
    province text,
    country text NOT NULL,
    date text NOT NULL,
    lat float,
    lon float,
    confirmed integer NOT NULL,
    dead integer NOT NULL,
    recovered integer NOT NULL,
    active integer NOT NULL,
    sync_date text NOT NULL,
    key text NOT NULL
);
CREATE UNIQUE INDEX IF NOT EXISTS cases_pkey ON cases(id int4_ops);