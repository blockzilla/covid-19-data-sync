CREATE TABLE IF NOT EXISTS countries (
    id SERIAL PRIMARY KEY,
    date text NOT NULL,
    country text NOT NULL,
    total_confirmed integer NOT NULL,
    total_dead integer NOT NULL,
    total_recovered integer NOT NULL,
    total_active integer NOT NULL,
    total_percentage integer NOT NULL
);
CREATE UNIQUE INDEX IF NOT EXISTS countries_pkey ON countries(id int4_ops);