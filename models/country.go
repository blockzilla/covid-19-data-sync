package models

// Country model
type Country struct {
	ID              int
	Date            string
	Country         string
	TotalConfirmed  int64
	TotalDead       int64
	TotalRecovered  int64
	TotalActive     int64
	TotalPercentage int64
}
