package models

// Case model
type Case struct {
	ID        int
	Town      string
	Province  string
	Country   string
	Date      string
	Lat       float64
	Lon       float64
	Confirmed int64
	Dead      int64
	Recovered int64
	Active    int64
	SyncDate  string
	Key       string
}
