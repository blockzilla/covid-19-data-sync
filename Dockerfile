FROM alpine:latest as certs
RUN apk --update add ca-certificates
ARG CERT_FILE=/etc/ssl/certs/ca-certificates.crt


FROM alpine:latest

ARG BUILD_NAME

COPY --from=certs $CERT_FILE $CERT_FILE

RUN apk update && \
    apk add bash
ADD /entrypoint.sh /
ADD /jobs /jobs
ADD /covid-19-data-sync /


ENTRYPOINT ["/entrypoint.sh"]
