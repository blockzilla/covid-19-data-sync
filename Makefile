NAME:=$(CI_PROJECT_NAME)
VERSION:=$(CI_COMMIT_REF_NAME)
BUILD_ARCH:=$(GO_BUILD_ARCH)

ifeq (${BUILD_ARCH},)
	# Looks like we are not running in the CI so default to current branch
	BUILD_ARCH:=linux
endif

ifeq (${VERSION},)
	# Looks like we are not running in the CI so default to current branch
	VERSION:=$(shell git rev-parse --abbrev-ref HEAD)
endif

ifeq ($(NAME),)
	# Looks like we are not running in the CI so default to current directory
	NAME:=$(notdir $(CURDIR))
endif


.PHONY: all build dep docs test docker-build docker-push docker-run run-dev deploy

all: build

build: ## Build golang binary for linux
	@ CGO_ENABLED=0 GOOS=${BUILD_ARCH} go build -mod=vendor -o ${NAME} -a -installsuffix cgo ./cmd/covid-19-data-sync

dep: ## Download golang dependencies
	@ go mod vendor

test: ## Run tests
	@ go test -mod=vendor -cover -race $(shell go list ./... | grep -v /vendor/)

docker-build: ## Build docker image with short git hash
	@ docker build -t blockzilla/${NAME}:${VERSION} .

docker-push: ## Upload image to repository
	@ docker push blockzilla/${NAME}:${VERSION}

docker-run: ## Upload image to repository
	@ docker run -it blockzilla/${NAME}:${VERSION}

run-dev: ## Run dev server
	go run ./cmd/covid-19-data-sync/main.go

dev: run-dev ## Builds and runs dev server
