#!/bin/bash
set -e

echo $GOOGLE_APPLICATION_CREDENTIALS_SECRET > /serviceaccount.json
export GOOGLE_APPLICATION_CREDENTIALS=/serviceaccount.json

mkdir -p /cases
export CASE_DATA_DIR=cases

/covid-19-data-sync