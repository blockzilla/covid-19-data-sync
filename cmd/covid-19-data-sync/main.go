package main

import (
	"context"
	"os"

	"github.com/alexflint/go-arg"
	"github.com/sirupsen/logrus"
	cmd "gitlab.com/blockzilla/covid-19-data-sync/cmd"
	"gitlab.com/blockzilla/covid-19-data-sync/pkg/database"
	"gitlab.com/blockzilla/covid-19-data-sync/pkg/dataproc"
	"gitlab.com/blockzilla/covid-19-data-sync/pkg/gsbucket"
	"gitlab.com/blockzilla/covid-19-data-sync/pkg/messaging"
	"gitlab.com/blockzilla/covid-19-data-sync/pkg/sync"
)

func main() {

	// load application config
	cfg := cmd.DefaultConfiguration()
	arg.MustParse(cfg)

	// initialise the queue
	var logger = logrus.New()
	// mc := initializeMessaging(cfg.AmqpConfig.ServerURL)
	// if mc != nil {
	// 	logger.Error("Cant initialise message queue")
	// }

	// initialise the database
	store := database.NewStore(database.Connect(cfg.DBConfig.DBUrl, logger), logger)

	// clean up google bucket from previous runs
	gsbucket.Delete(cfg.Cases.Directory, cfg.BucketName)
	gsbucket.Delete(cfg.PysparkJobs.CaseData, cfg.BucketName)
	gsbucket.Delete(cfg.PysparkGeoCodingJobs.GeoCodingData, cfg.BucketName)

	// // scrape case data from github repo
	if sync.ScrapeCaseData(cfg) {

		// cleanup old jobs
		gsbucket.Delete(cfg.PigJobs.CleansedData, cfg.BucketName)

		// add data processing jobs files to google bucket
		gsbucket.Write(cfg.PysparkJobs.Job, cfg.BucketName, cfg)
		gsbucket.Write(cfg.PigJobs.Job, cfg.BucketName, cfg)
		gsbucket.Write(cfg.PysparkGeoCodingJobs.Job, cfg.BucketName, cfg)

		// run the data processing jobs
		dataproc.DataProcessPipeline(cfg)

		// get processed data from bucket
		countryData, err := gsbucket.Get(cfg.PysparkJobs.CountryData, "part-", cfg.BucketName, cfg)
		if countryData != nil && err == nil {
			// insert processed data into database
			err = store.Countries.ProcessCountries(context.Background(), countryData)
			if err != nil {
				logger.Error(err)
				os.Exit(0)
			}
		}

		// get processed data from bucket
		caseData, err := gsbucket.Get(cfg.PysparkJobs.CaseData, "part-", cfg.BucketName, cfg)
		if caseData != nil && err == nil {
			// insert processed data into database
			err = store.Cases.ProcessCases(context.Background(), caseData)
			if err != nil {
				logger.Error(err)
				os.Exit(0)
			}

			// check for missing geo codes
			missingGeoCode, goErr := store.Cases.GeoCodeMissing(context.Background())
			if goErr != nil {
				logger.Error(goErr)
				os.Exit(0)
			}

			// if we have records missing geo coding push to a google bucket for processing
			if missingGeoCode != nil {
				bucErr := sync.PushGeoToBucket(cfg, missingGeoCode)

				if bucErr != nil {
					logger.Error(bucErr)
					os.Exit(0)
				}

				// run the geocoding process
				dataproc.GeoCodingPipeline(cfg)

				// grab results and write to the database
				geoCodedCaseData, err := gsbucket.Get(cfg.PysparkGeoCodingJobs.OutputData, "part-", cfg.BucketName, cfg)
				if geoCodedCaseData != nil && err == nil {
					// insert processed data into database
					err = store.Cases.ProcessGeoCoding(context.Background(), geoCodedCaseData)
					if err != nil {
						logger.Error(err)
						os.Exit(0)
					}
				}
			}
		}
	}

	// clean up bucket
	// gsbucket.Delete(cfg.PigJobs.CleansedData, cfg.BucketName)

}

// initialise the messaging client
func initializeMessaging(serverURL string) *messaging.AmqpClient {
	if serverURL == "" {
		panic("No 'amqp_server_url' set in configuration, cannot start")
	}

	mc := &messaging.AmqpClient{}
	mc.ConnectToBroker(serverURL)

	return mc
}
