package cmd

// Config the config for the app
type Config struct {
	BucketName string `arg:"env:GOOGLE_STORAGE_BUCKET"`
	GitRepo    string `arg:"env:GIT_REPO"`
	FilePrefix string `arg:"env:FILE_PREFIX"`
	SyncAmount string `arg:"env:SYNC_AMOUNT"`
	Cases
	Dataproc
	PysparkJobs
	PysparkGeoCodingJobs
	PigJobs
	AmqpConfig
	DBConfig
}

// Cases directory for case data
type Cases struct {
	Directory string `arg:"env:CASE_DATA_DIR"`
}

// Dataproc config
type Dataproc struct {
	ProjectID   string `arg:"env:DATAPROC_PROJECT_ID"`
	ClusterName string `arg:"env:DATAPROC_CLUSTER_NAME"`
	Region      string `arg:"env:DATAPROC_REGION"`
}

// PysparkJobs config
type PysparkJobs struct {
	Job         string `arg:"env:PYSPARK_JOB_FILE"`
	CaseData    string
	CountryData string
}

// PysparkGeoCodingJobs config
type PysparkGeoCodingJobs struct {
	Job           string `arg:"env:PYSPARK_GEOCODE_JOB_FILE"`
	GeoCodingData string
	GeoCodingFile string
	OutputData    string
}

// PigJobs config
type PigJobs struct {
	Job          string `arg:"env:PIG_JOB_FILE"`
	CleansedData string
}

// AmqpConfig ActiveMQ connection details
type AmqpConfig struct {
	// serverURL amqp connection string
	ServerURL string `arg:"env:AMQP_SERVER_URL"`
	LogQueue  string
}

// DBConfig database config
type DBConfig struct {
	// Database url
	DBUrl string `arg:"env:DB_URL"`
}

// DefaultConfiguration returns the default config
func DefaultConfiguration() *Config {
	return &Config{
		BucketName: "corona-data-bucket1",
		GitRepo:    "https://github.com/CSSEGISandData/COVID-19",
		FilePrefix: "csse_covid_19_data/csse_covid_19_daily_reports/",
		SyncAmount: "daily",
		Cases: Cases{
			Directory: "cases/",
		},
		Dataproc: Dataproc{
			ProjectID:   "coronavirus-271700",
			ClusterName: "coronaworld",
			Region:      "europe-west4",
		},
		PysparkJobs: PysparkJobs{
			Job:         "jobs/covid-cases.py",
			CaseData:    "alldata/",
			CountryData: "distinct/",
		},
		PysparkGeoCodingJobs: PysparkGeoCodingJobs{
			Job:           "jobs/geo-coding.py",
			GeoCodingData: "geocoding/",
			GeoCodingFile: "geocoding.csv",
			OutputData:    "geoout/",
		},
		PigJobs: PigJobs{
			Job:          "jobs/process-nulls.pig",
			CleansedData: "cleansed/",
		},
		AmqpConfig: AmqpConfig{
			ServerURL: "amqp://guest:guest@rabbitmq:5672/",
			LogQueue:  "job-log",
		},
		DBConfig: DBConfig{
			DBUrl: "postgresql://root:password@127.0.0.1:5432/database?sslmode=enable",
		},
	}
}
