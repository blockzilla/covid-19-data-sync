import sys
import getopt
import requests
import time
import csv
import os
import logging
import sys
from io import StringIO
from pyspark.ml import Pipeline
from pyspark.ml.classification import LogisticRegression
from pyspark.ml.feature import HashingTF, Tokenizer
from pyspark.sql import Row
from pyspark.sql.functions import UserDefinedFunction
from pyspark.sql.types import StringType, DateType, IntegerType, StructType, StructField
from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.functions import udf, col, lit, lower, regexp_replace, sum, concat, split
from pyspark.sql.window import Window

API_KEY = "AIzaSyAVwOn-eJq8pjrid5zyJsxHO3IvNjbD2SY"
inputFiles = "gs://corona-data-bucket1/geocoding"
geoOutFiles = "gs://corona-data-bucket1/geoout"

sc = SparkContext.getOrCreate()
largeList = []
partitionNum = 100
sc.parallelize(largeList, partitionNum)
sqlContext = SQLContext(sc)

# schema for the data
schema = StructType([
    StructField("town", StringType(), True),
    StructField("province", StringType(), True),
    StructField("country", StringType(), False),
    StructField("date", DateType(), False),
    StructField("lat", StringType(), True),
    StructField("lon", StringType(), True),
    StructField("confirmed", IntegerType(), True),
    StructField("dead", IntegerType(), True),
    StructField("recovered", IntegerType(), True),
    StructField("active", IntegerType(), True),
    StructField("sync_date", StringType(), True),
    StructField("key", StringType(), True)]
)

# Load all data
df = sqlContext.read.csv(inputFiles,
                         header=True, schema=schema)

df.show()


def get_google_results(address):
    # total hack should not be committed here
    apiKey = "AIzaSyAVwOn-eJq8pjrid5zyJsxHO3IvNjbD2SY"
    # Set up your Geocoding url
    geocode_url = "https://maps.googleapis.com/maps/api/geocode/json?address={}".format(
        address)
    if apiKey is not None:
        geocode_url = geocode_url + "&key={}".format(apiKey)

    output = ""
    results = requests.get(geocode_url)
    results = results.json()

    # if there's no results or an error, return empty results.
    if len(results['results']) == 0:
        output = "n/a"
    else:
        answer = results['results'][0]
        output = str(answer.get('geometry').get('location').get(
            'lat')) + "," + str(answer.get('geometry').get('location').get('lng'))

    return output


def geoCode(country, province, town):
    townStr = str(town)
    provinceStr = str(province)
    countryStr = str(country)
    locationStr = ""

    if (townStr != "Unassigned" or townStr != "None" or townStr != "n/a" or townStr != "nan" or townStr != "null") and len(townStr) != 0:
        locationStr += townStr + ", "
    if (province != "Northern Mariana Islands" or province != "Recovered" or province != "Diamond Princess" or province != "None" or province != "n/a" or province != "nan" or province != "null") and len(province) != 0:
        locationStr += province + ", "

    locationStr += country
    return get_google_results(locationStr)


udfgeoCode = udf(geoCode, StringType())

df = df.drop('lat').drop('lon')

geoCodedResults = df.withColumn(
    "geo", udfgeoCode("country", "province", "town"))

geoCodedResults.show()

dfGeoCoded = geoCodedResults.withColumn("lat", split(col("geo"), ",").getItem(
    0)).withColumn("lon", split(col("geo"), ",").getItem(1)).drop("geo")
dfGeoCoded.show()

dfSorted = dfGeoCoded.select(
    "town", "province", "country", "date", "lat", "lon", "confirmed", "dead", "recovered", "active", "sync_date", "key")

dfSorted.repartition(1).write.format(
    'com.databricks.spark.csv').option("header", "true").mode('overwrite').save(geoOutFiles, format='csv')
