--Load data
cases = LOAD 'gs://corona-data-bucket1/cases/*.csv' USING org.apache.pig.piggybank.storage.CSVExcelStorage(',', 'YES_MULTILINE', 'NOCHANGE', 'SKIP_INPUT_HEADER')
AS(
    town: chararray,
    province: chararray,
    country: chararray,
    date: chararray,
    lat: chararray,
    lon: chararray,
    confirmed: int,
    dead: int,
    recovered: int,
    active: int,
    sync_date: chararray
);

cases_no_headers = FILTER cases BY town!='town';

-- sanatise empty fields and blank columns
cases_no_headers = foreach cases_no_headers generate  ( town != '' ? town : 'n/a') as town
                                        , (province != '' ? province : 'n/a') as province
                                        , country
                                        , date
                                        , lat
                                        , lon
                                        , (confirmed IS NOT NULL ? confirmed : 0) as confirmed
                                        , (dead IS NOT NULL ? dead : 0) as dead
                                        , (recovered IS NOT NULL ? recovered : 0) as recovered
                                        , (active IS NOT NULL ? active : 0) as active
                                        , sync_date
                                    ;
-- THIS IS NOT IDEAL BUT DONT HAVE TIME TO CORRECT IT
-- The data source has bad data and as it updates daily
-- and the format is constantly changing this was the quickes way to deal with it
-- not efficient :-(
cases_no_headers = foreach cases_no_headers generate town
                                        , (province == 'Grand Princess' ? 'n/a' : province ) as province
                                        , country
                                        , date
                                        , lat
                                        , lon
                                        , confirmed
                                        , dead
                                        , recovered
                                        , active
                                        , sync_date
                                    ;
-- not efficient :-(
final_output = foreach cases_no_headers generate town
                                        , (province == 'Dimond Princess' ? 'n/a' : province ) as province
                                        , country
                                        , date
                                        , lat
                                        , lon
                                        , confirmed
                                        , dead
                                        , recovered
                                        , active
                                        , sync_date
                                    ;
-- not efficient :-(
final_output = foreach cases_no_headers generate town
                                        , (province == 'Diamond Princess' ? 'n/a' : province ) as province
                                        , country
                                        , date
                                        , lat
                                        , lon
                                        , confirmed
                                        , dead
                                        , recovered
                                        , active
                                        , sync_date
                                    ;
-- not efficient :-(
final_output = foreach cases_no_headers generate town
                                        , (province == 'Recovered' ? 'n/a' : province ) as province
                                        , country
                                        , date
                                        , lat
                                        , lon
                                        , confirmed
                                        , dead
                                        , recovered
                                        , active
                                        , sync_date
                                    ;
-- not efficient :-(
final_output = foreach cases_no_headers generate town
                                        , (province == 'Northern Mariana Islands' ? 'n/a' : province ) as province
                                        , country
                                        , date
                                        , lat
                                        , lon
                                        , confirmed
                                        , dead
                                        , recovered
                                        , active
                                        , sync_date
                                    ;

-- dave the data for further processing
STORE final_output into 'gs://corona-data-bucket1/cleansed' USING org.apache.pig.piggybank.storage.CSVExcelStorage(',','NO_MULTILINE','NOCHANGE', 'WRITE_OUTPUT_HEADER');
