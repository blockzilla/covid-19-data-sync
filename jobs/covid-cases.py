import sys
import getopt
import requests
import time
import csv
import os
import logging
import sys
from io import StringIO
from pyspark.ml import Pipeline
from pyspark.ml.classification import LogisticRegression
from pyspark.ml.feature import HashingTF, Tokenizer
from pyspark.sql import Row
from pyspark.sql.functions import UserDefinedFunction
from pyspark.sql.types import StringType, DateType, IntegerType, StructType, StructField
from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.functions import udf, col, lit, lower, regexp_replace, sum, concat
from pyspark.sql.window import Window

inputFiles = "gs://corona-data-bucket1/cleansed/part-m-00000"
distinctFiles = "gs://corona-data-bucket1/distinct"
allData = "gs://corona-data-bucket1/alldata"
LIMIT_COUNTER = 0

sc = SparkContext.getOrCreate()
largeList = []
partitionNum = 100
sc.parallelize(largeList, partitionNum)
sqlContext = SQLContext(sc)

# schema for the data
schema = StructType([
    StructField("town", StringType(), True),
    StructField("province", StringType(), True),
    StructField("country", StringType(), False),
    StructField("date", DateType(), False),
    StructField("lat", StringType(), True),
    StructField("lon", StringType(), True),
    StructField("confirmed", IntegerType(), True),
    StructField("dead", IntegerType(), True),
    StructField("recovered", IntegerType(), True),
    StructField("active", IntegerType(), True),
    StructField("sync_date", StringType(), True)]
)

# Load all data
df = sqlContext.read.csv(inputFiles,
                         header=True, schema=schema)

df.show()

# remove rows with no case data, pick rows with any data
df = df.where((col('confirmed') > 0) | (
    col('dead') > 0) | (col('recovered') > 0))
df.show()


# create key of the town, province, country column, and clean up some junk data
dfWithKey = df.withColumn(
    "key", regexp_replace(regexp_replace(regexp_replace(lower(concat(col('town'), lit(
        '_'), col('province'), lit('_'), col('country'))), ' ', '_'), ',', ''), 'n/a_', '')).drop('active')
dfWithKey.show()

# calculate the amount of active cases
dfWithActiveCases = dfWithKey.withColumn('active', col('confirmed') -
                                         (col('dead') + col('recovered')))
dfWithActiveCases.show()

# sort the data
dfWithActiveCasesSort = dfWithActiveCases.select(
    "town", "province", "country", "date", "lat", "lon", "confirmed", "dead", "recovered", "active", "sync_date", "key")

# all data
dfWithActiveCasesSort.repartition(1).write.format(
    'com.databricks.spark.csv').option("header", "true").mode('overwrite').save(allData, format='csv')


# Calculate the country totals per day
dfAgg = dfWithActiveCasesSort.groupby("date", "country").\
    agg(
        sum("confirmed").alias("total_confirmed"),
        sum("dead").alias("total_dead"),
        sum("recovered").alias("total_recovered"),
        sum("active").alias("total_active")
).drop('unique_countries').orderBy('date')


# sum and counts
dfAgg.repartition(1).write.format(
    'com.databricks.spark.csv').option("header", "true").mode('overwrite').save(distinctFiles, format='csv')
