module gitlab.com/blockzilla/covid-19-data-sync

go 1.13

require (
	cloud.google.com/go v0.53.0
	cloud.google.com/go/storage v1.6.0
	github.com/alexflint/go-arg v1.3.0
	github.com/sirupsen/logrus v1.4.2
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	google.golang.org/api v0.18.0
	google.golang.org/genproto v0.0.0-20200224152610-e50cd9704f63
	gopkg.in/src-d/go-billy.v4 v4.3.2
	gopkg.in/src-d/go-git.v4 v4.13.1
)
