package gsbucket

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"github.com/sirupsen/logrus"

	cmd "gitlab.com/blockzilla/covid-19-data-sync/cmd"
	"google.golang.org/api/iterator"

	"cloud.google.com/go/storage"
)

// Get get data from the bucket
func Get(object, filePrefix, bucket string, cfg *cmd.Config) ([]byte, error) {
	logrus.Info(fmt.Sprintf("Reading files from %s Google Bucket %s", object, bucket))

	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		logrus.Fatal(err)
	}

	ctx, cancel := context.WithTimeout(ctx, time.Second*10)
	defer cancel()
	it := client.Bucket(bucket).Objects(ctx, &storage.Query{
		Prefix:    object,
		Delimiter: "/",
	})

	// loop through bucket files
	for {
		attrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil || attrs == nil {
		} else {
			logrus.Println(attrs.Name)

			if strings.HasPrefix(attrs.Name, fmt.Sprintf("%s%s", object, filePrefix)) {
				// download the latest file
				o, err := client.Bucket(bucket).Object(attrs.Name).NewReader(ctx)
				if err != nil {
					return nil, err
				}
				defer o.Close()

				data, err := ioutil.ReadAll(o)
				if err != nil {
					return nil, err
				}

				return data, nil
			}
		}
	}
	return nil, nil
}

// Write the file to google datastore
func Write(object, bucket string, cfg *cmd.Config) {

	logrus.Info(fmt.Sprintf("Pushing %s to Google Bucket %s", object, bucket))

	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		logrus.Fatal(err)
	}

	f, err := os.Open(object)
	if err != nil {
		logrus.Fatal(err)
	}
	defer f.Close()

	ctx, cancel := context.WithTimeout(ctx, time.Second*50)
	defer cancel()
	wc := client.Bucket(bucket).Object(object).NewWriter(ctx)
	if _, err = io.Copy(wc, f); err != nil {
		logrus.Fatal(err)
	}
	if err := wc.Close(); err != nil {
		logrus.Fatal(err)
	}
}

// Delete cleans up
func Delete(object, bucket string) {
	logrus.Info(fmt.Sprintf("Deleting %s from Google Bucket %s", object, bucket))

	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		logrus.Fatal(err)
	}

	ctx, cancel := context.WithTimeout(ctx, time.Second*10)
	defer cancel()
	it := client.Bucket(bucket).Objects(ctx, &storage.Query{
		Prefix: object,
	})
	for {
		attrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil || attrs == nil {

		} else {
			logrus.Println(attrs.Name)
			ctx, cancel := context.WithTimeout(ctx, time.Second*10)

			defer cancel()

			o := client.Bucket(bucket).Object(attrs.Name)
			if err := o.Delete(ctx); err != nil {
				logrus.Println(err)
			}
		}
	}
}
