package sync

import (
	"encoding/csv"
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	cmd "gitlab.com/blockzilla/covid-19-data-sync/cmd"
	"gitlab.com/blockzilla/covid-19-data-sync/models"
	"gitlab.com/blockzilla/covid-19-data-sync/pkg/gsbucket"
	"gitlab.com/blockzilla/covid-19-data-sync/pkg/utils"
)

// PushGeoToBucket write file to disk
func PushGeoToBucket(cfg *cmd.Config, cases []*models.Case) error {
	gsObjectName := fmt.Sprintf("%s%s", cfg.PysparkGeoCodingJobs.GeoCodingData, cfg.PysparkGeoCodingJobs.GeoCodingFile)
	csvdatafile, err := os.Create(gsObjectName)

	if err != nil {
		logrus.Error(err)
	}
	defer csvdatafile.Close()

	writer := csv.NewWriter(csvdatafile)

	var csvHeader []string
	csvHeader = append(csvHeader, "town")
	csvHeader = append(csvHeader, "province")
	csvHeader = append(csvHeader, "country")
	csvHeader = append(csvHeader, "date")
	csvHeader = append(csvHeader, "lat")
	csvHeader = append(csvHeader, "lon")
	csvHeader = append(csvHeader, "confirmed")
	csvHeader = append(csvHeader, "dead")
	csvHeader = append(csvHeader, "recovered")
	csvHeader = append(csvHeader, "active")
	csvHeader = append(csvHeader, "sync_date")
	csvHeader = append(csvHeader, "key")
	writer.Write(csvHeader)

	for _, caseObj := range cases {
		var csvRecord []string
		csvRecord = append(csvRecord, caseObj.Town)
		csvRecord = append(csvRecord, caseObj.Province)
		csvRecord = append(csvRecord, caseObj.Country)
		csvRecord = append(csvRecord, caseObj.Date)
		csvRecord = append(csvRecord, utils.FloatToString(caseObj.Lat))
		csvRecord = append(csvRecord, utils.FloatToString(caseObj.Lon))
		csvRecord = append(csvRecord, utils.Int64ToString(caseObj.Confirmed))
		csvRecord = append(csvRecord, utils.Int64ToString(caseObj.Dead))
		csvRecord = append(csvRecord, utils.Int64ToString(caseObj.Recovered))
		csvRecord = append(csvRecord, utils.Int64ToString(caseObj.Active))
		csvRecord = append(csvRecord, caseObj.SyncDate)
		csvRecord = append(csvRecord, caseObj.Key)

		writer.Write(csvRecord)
	}

	writer.Flush()

	// push csv data to google bucket
	gsbucket.Write(gsObjectName, cfg.BucketName, cfg)

	return nil
}
