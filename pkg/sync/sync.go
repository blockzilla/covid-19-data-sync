package sync

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	"github.com/sirupsen/logrus"

	cmd "gitlab.com/blockzilla/covid-19-data-sync/cmd"
	"gitlab.com/blockzilla/covid-19-data-sync/pkg/gsbucket"
	"gitlab.com/blockzilla/covid-19-data-sync/pkg/utils"

	"gopkg.in/src-d/go-billy.v4/memfs"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"gopkg.in/src-d/go-git.v4/storage/memory"

	. "gopkg.in/src-d/go-git.v4/_examples"
)

/**
	This file syncs data from GitHub repository
	https://github.com/CSSEGISandData/COVID-19

	Some mild data manipulaiton occours while processing
	the data format changes over the course of time
	so we massage the dates format at source
**/

const (
	gsObjectNameTmpl = "covid-19-data-%s.csv"
)

var (
	executeJob = false
)

// ScrapeCaseData scrapes data from github
func ScrapeCaseData(cfg *cmd.Config) bool {
	logrus.Infof("Initialised config, data scrape starting...")

	// clone git repo into memory
	fs := memfs.New()

	logrus.Infof("Cloning data repository...")
	// mc.LogMessage("Cloning COVID-19 data repository", cfg.AmqpConfig.LogQueue)

	repo, err := git.Clone(memory.NewStorage(), fs, &git.CloneOptions{
		URL: cfg.GitRepo,
	})

	// retrieving the branch being pointed by HEAD
	ref, err := repo.Head()
	CheckIfError(err)

	// retrieving the commit object
	commit, err := repo.CommitObject(ref.Hash())
	CheckIfError(err)
	logrus.Println(commit)

	// retrieve the tree from the commit
	tree, err := commit.Tree()
	CheckIfError(err)

	// get the files iterator and print the file
	logrus.Infof("Process CSV files in data repository...")
	tree.Files().ForEach(func(f *object.File) error {
		filePath := f.Name

		// Filter out files we dont care about
		if strings.HasPrefix(filePath, cfg.FilePrefix) {

			// filter for CSV files and strip the extension
			fileName := utils.After(filePath, cfg.FilePrefix)
			fileExtension := utils.After(fileName, ".")
			fileName = utils.Before(fileName, ".")

			// get the current data and sub 1
			// We run the sync job After midnight on yesterdays reports
			// todayStr := "02/01/2020" // MM-DD-YYYY
			todayStr := time.Now().AddDate(0, 0, -1).Format("01-02-2006") // MM-DD-YYYY

			// in most cases we only want the lastest data file, otherwise sync all files
			if (cfg.SyncAmount == "daily" && todayStr == fileName) || cfg.SyncAmount == "all" {

				// only process csv files
				if fileExtension == "csv" {

					gsObjectName := fmt.Sprintf("%s%s", cfg.Cases.Directory, fmt.Sprintf(gsObjectNameTmpl, fileName))
					csvdatafile, err := os.Create(gsObjectName)

					if err != nil {
						fmt.Println(err)
					}
					defer csvdatafile.Close()

					writer := csv.NewWriter(csvdatafile)

					// load validated file paths
					file, err := fs.Open(filePath)

					if err != nil {
						logrus.Print(err)
					}

					// Parse the file
					r := csv.NewReader(file)

					logrus.Infof("Running daily data scrape: ", fileName)

					var firstRow = true
					var format = ""
					// Iterate through the record rows
					for {
						// process each record from csv
						record, err := r.Read()
						if err == io.EOF {
							break
						}
						if err != nil {
							logrus.Fatal(err)
						}

						// the data is messy and keeps changing
						if firstRow {
							if record[3] == "Confirmed" && len(record) == 6 {
								format = "0"
							} else if record[3] == "Confirmed" && len(record) == 8 {
								format = "1"
							} else if record[0] == "FIPS" {
								format = "2"
							}
						}

						// cheaply skip row 0, hacky check if a number is a string
						if firstRow == true {
							firstRow = false
							var csvRecord []string
							csvRecord = append(csvRecord, "town")
							csvRecord = append(csvRecord, "province")
							csvRecord = append(csvRecord, "country")
							csvRecord = append(csvRecord, "date")
							csvRecord = append(csvRecord, "lat")
							csvRecord = append(csvRecord, "lon")
							csvRecord = append(csvRecord, "confirmed")
							csvRecord = append(csvRecord, "dead")
							csvRecord = append(csvRecord, "recovered")
							csvRecord = append(csvRecord, "active")
							csvRecord = append(csvRecord, "sync_date")
							writer.Write(csvRecord)
						} else if format == "0" {
							/// Province/State	Country/Region	Last Update	Confirmed	Deaths	Recovered
							var csvRecord []string
							csvRecord = append(csvRecord, utils.NilOrString(""))
							csvRecord = append(csvRecord, utils.NilOrString(record[0]))
							csvRecord = append(csvRecord, utils.NilOrString(record[1]))
							csvRecord = append(csvRecord, utils.NilOrString(utils.SanatiseDate(record[2]).String()))
							csvRecord = append(csvRecord, utils.NilOrString(""))
							csvRecord = append(csvRecord, utils.NilOrString(""))
							csvRecord = append(csvRecord, utils.NilOrString(record[3]))
							csvRecord = append(csvRecord, utils.NilOrString(record[4]))
							csvRecord = append(csvRecord, utils.NilOrString(record[5]))
							csvRecord = append(csvRecord, utils.NilOrString("0"))
							csvRecord = append(csvRecord, utils.NilOrString(todayStr))
							writer.Write(csvRecord)
						} else if format == "1" {
							/// Province/State	Country/Region	Last Update	Confirmed	Deaths	Recovered	Latitude	Longitude
							var csvRecord []string
							csvRecord = append(csvRecord, utils.NilOrString(""))
							csvRecord = append(csvRecord, utils.NilOrString(record[0]))
							csvRecord = append(csvRecord, utils.NilOrString(record[1]))
							csvRecord = append(csvRecord, utils.NilOrString(utils.SanatiseDate(record[2]).String()))
							csvRecord = append(csvRecord, utils.NilOrString(record[6]))
							csvRecord = append(csvRecord, utils.NilOrString(record[7]))
							csvRecord = append(csvRecord, utils.NilOrString(record[3]))
							csvRecord = append(csvRecord, utils.NilOrString(record[4]))
							csvRecord = append(csvRecord, utils.NilOrString(record[5]))
							csvRecord = append(csvRecord, utils.NilOrString("0"))
							csvRecord = append(csvRecord, utils.NilOrString(todayStr))
							writer.Write(csvRecord)
						} else if format == "2" {
							// FIPS	Admin2	Province_State	Country_Region	Last_Update	Lat	Long_	Confirmed	Deaths	Recovered	Active	Combined_Key
							var csvRecord []string
							csvRecord = append(csvRecord, utils.NilOrString(record[1]))
							csvRecord = append(csvRecord, utils.NilOrString(record[2]))
							csvRecord = append(csvRecord, utils.NilOrString(record[3]))
							csvRecord = append(csvRecord, utils.NilOrString(utils.SanatiseDate(record[4]).String()))
							csvRecord = append(csvRecord, utils.NilOrString(record[5]))
							csvRecord = append(csvRecord, utils.NilOrString(record[6]))
							csvRecord = append(csvRecord, utils.NilOrString(record[7]))
							csvRecord = append(csvRecord, utils.NilOrString(record[8]))
							csvRecord = append(csvRecord, utils.NilOrString(record[9]))
							csvRecord = append(csvRecord, utils.NilOrString("0"))
							csvRecord = append(csvRecord, utils.NilOrString(todayStr))
							writer.Write(csvRecord)
						}
					}
					writer.Flush()
					logrus.Println(gsObjectName)

					// push csv data to google bucket
					gsbucket.Write(gsObjectName, cfg.BucketName, cfg)

					if cfg.SyncAmount == "all" {
						time.Sleep(2 * time.Second)
					}

					executeJob = true
					logrus.Infof("Data sync successful")
				}
			}
		}
		return nil
	})

	return executeJob
}
