package database

import (
	"context"
	"encoding/csv"
	"io"
	"io/ioutil"
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/blockzilla/covid-19-data-sync/models"
	"gitlab.com/blockzilla/covid-19-data-sync/pkg/utils"
)

// Cases interface methods for manipulating case tables
type Cases interface {
	CreateCases(context.Context, *models.Case) error
	Update(context.Context, *models.Case) error
	ProcessCases(context.Context, []byte) error
	ProcessGeoCoding(context.Context, []byte) error
	GeoCodeMissing(context.Context) ([]*models.Case, error)
}

// CasesSQL interface methods for manipulating data tables
type CasesSQL struct {
	db     *DBClient
	logger *logrus.Logger
}

// CreateCases create data in the database
func (s *CasesSQL) CreateCases(context context.Context, cases *models.Case) error {

	logrus.Infof(`%s, %s, %s, %s, %d, %d, %d, %d, %d, %d, %s, %s`,
		cases.Town,
		cases.Province,
		cases.Country,
		cases.Date,
		cases.Lat,
		cases.Lon,
		cases.Confirmed,
		cases.Dead,
		cases.Recovered,
		cases.Active,
		cases.SyncDate,
		cases.Key,
	)

	return s.db.ExecWithArgs(
		context,
		`INSERT INTO cases(
			town,
			province,
			country,
			date,
			lat,
			lon,
			confirmed,
			dead,
			recovered,
			active,
			sync_date,
			key
		)
		values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)`,
		cases.Town,
		cases.Province,
		cases.Country,
		cases.Date,
		cases.Lat,
		cases.Lon,
		cases.Confirmed,
		cases.Dead,
		cases.Recovered,
		cases.Active,
		cases.SyncDate,
		cases.Key,
	)
}

// ProcessGeoCoding work the geo coding records and update db
func (s *CasesSQL) ProcessGeoCoding(ctx context.Context, d []byte) error {
	if d != nil {
		fileName := "geo-case-data.csv"

		err := ioutil.WriteFile(fileName, d, 0644)
		if err != nil {
			return err
		}

		file, err := os.Open(fileName)
		if err != nil {
			logrus.Fatalln("Couldn't open the csv file", err)
			return err
		}

		// Parse the file
		r := csv.NewReader(file)
		logrus.Info("Writing data to database")

		var firstRow = true
		// Iterate through the record rows
		for {
			// process each record from csv
			record, err := r.Read()
			if err == io.EOF {
				break
			}
			if err != nil {
				return err
			}
			if firstRow == true {
				firstRow = false
			} else {
				err = s.Update(
					context.Background(),
					&models.Case{
						Town:      record[0],
						Province:  record[1],
						Country:   record[2],
						Date:      record[3],
						Lat:       utils.StringToFloat64(record[4]),
						Lon:       utils.StringToFloat64(record[5]),
						Confirmed: utils.StringToInt64(record[6]),
						Dead:      utils.StringToInt64(record[7]),
						Recovered: utils.StringToInt64(record[8]),
						Active:    utils.StringToInt64(record[9]),
						SyncDate:  record[10],
						Key:       record[11],
					},
				)
				if err != nil {
					logrus.Error(err.Error())
					return err
				}
			}
		}
	}
	logrus.Info("Database update complete")
	return nil
}

// Update a case
func (s *CasesSQL) Update(context context.Context, cases *models.Case) error {
	return s.db.ExecWithArgs(
		context,
		`UPDATE cases SET(
			town,
			province,
			country,
			date,
			lat,
			lon,
			confirmed,
			dead,
			recovered,
			active,
			sync_date,
			key
		) = ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12) WHERE key = $13 AND date = $14`,
		cases.Town,
		cases.Province,
		cases.Country,
		cases.Date,
		cases.Lat,
		cases.Lon,
		cases.Confirmed,
		cases.Dead,
		cases.Recovered,
		cases.Active,
		cases.SyncDate,
		cases.Key,
		cases.Key,
		cases.Date,
	)
}

// ProcessCases write data to database for each case
func (s *CasesSQL) ProcessCases(ctx context.Context, d []byte) error {
	if d != nil {
		fileName := "case-data.csv"
		// fileName := "alldata_part-00000-9c5bcd09-7c3a-4a42-a9b5-68ffffbc35e5-c000.csv"

		err := ioutil.WriteFile(fileName, d, 0644)
		if err != nil {
			return err
		}

		file, err := os.Open(fileName)
		if err != nil {
			logrus.Fatalln("Couldn't open the csv file", err)
			return err
		}

		// Parse the file
		r := csv.NewReader(file)
		logrus.Info("Writing data to database")

		var firstRow = true
		// Iterate through the record rows
		for {
			// process each record from csv
			record, err := r.Read()
			if err == io.EOF {
				break
			}
			if err != nil {
				return err
			}
			if firstRow == true {
				firstRow = false
			} else {
				err = s.CreateCases(
					context.Background(),
					&models.Case{
						Town:      record[0],
						Province:  record[1],
						Country:   record[2],
						Date:      record[3],
						Lat:       utils.StringToFloat64(record[4]),
						Lon:       utils.StringToFloat64(record[5]),
						Confirmed: utils.StringToInt64(record[6]),
						Dead:      utils.StringToInt64(record[7]),
						Recovered: utils.StringToInt64(record[8]),
						Active:    utils.StringToInt64(record[9]),
						SyncDate:  record[10],
						Key:       record[11],
					},
				)
				if err != nil {
					logrus.Error(err.Error())
					return err
				}
			}
		}
	}
	logrus.Info("Database update complete")
	return nil
}

// GeoCodeMissing find missing geo codes
func (s *CasesSQL) GeoCodeMissing(ctx context.Context) ([]*models.Case, error) {
	logrus.Info("Reading cases with missing geocoding")

	caseList := []*models.Case{}
	cases, err := s.db.Query(ctx, "SELECT * FROM cases WHERE lat = 0 AND lon = 0")

	for cases.Next() {
		caseObj := &models.Case{}

		err := cases.Scan(
			&caseObj.ID,
			&caseObj.Town,
			&caseObj.Province,
			&caseObj.Country,
			&caseObj.Date,
			&caseObj.Lat,
			&caseObj.Lon,
			&caseObj.Confirmed,
			&caseObj.Dead,
			&caseObj.Recovered,
			&caseObj.Active,
			&caseObj.SyncDate,
			&caseObj.Key,
		)
		if err != nil {
			return caseList, err
		}
		caseList = append(caseList, caseObj)
	}
	cases.Close()
	return caseList, err
}
