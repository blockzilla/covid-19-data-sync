package database

import "github.com/sirupsen/logrus"

// Store is a collection of database interfaces
type Store struct {
	Cases     Cases
	Countries Countries
}

// NewStore creates a new instance of the database interfaces
func NewStore(db *DBClient, logger *logrus.Logger) *Store {
	return &Store{
		Cases: &CasesSQL{
			db:     db,
			logger: logger,
		},
		Countries: &CountriesSQL{
			db:     db,
			logger: logger,
		},
	}
}
