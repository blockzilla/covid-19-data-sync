package database

import (
	"context"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/sirupsen/logrus"
)

// Client defines our interface for connecting and consuming messages.
type Client interface {
	Connect(connectionString string, logger *logrus.Logger)
	Close()
}

// DBClient is our real implementation, encapsulates a pointer to an pgx
type DBClient struct {
	pool   *pgxpool.Pool
	logger *logrus.Logger
}

// Connect to a database using the supplied connectionString.
func Connect(connectionString string, logger *logrus.Logger) *DBClient {
	if connectionString == "" {
		panic("Cannot initialize connection to database, connectionString not set. Have you initialized?")
	}

	poolConfig, err := pgxpool.ParseConfig(connectionString)
	if err != nil {
		panic("Unable to parse DB_URL")
	}

	// TODO: configure db tuning and make environment optional variables with sensible defaults
	// poolConfig.HealthCheckPeriod = 1
	// poolConfig.MaxConnIdleTime = 30
	poolConfig.MaxConns = 20
	poolConfig.MinConns = 15

	// TODO: add databse logger

	conn, err := pgxpool.ConnectConfig(context.Background(), poolConfig)
	if err != nil {
		panic("Unable to connect to database:")
	}

	logger.Info("Database connected!")

	logger.Info("Processing database migrations")
	m, err := migrate.New(
		"file://db/migrations",
		connectionString,
	)
	if err != nil {
		logger.Fatalf("Error processing database migrations: %s", err)
	}
	// TODO: migrate DB down
	if err := m.Up(); err != nil && err.Error() != "no change" {
		logger.Fatalf("Error processing database migrations: %s", err)
	}
	logger.Info("Database migrated")

	return &DBClient{
		pool:   conn,
		logger: logger,
	}
}

// ExecWithArgs run a query against the database
func (c *DBClient) ExecWithArgs(context context.Context, sql string, args ...interface{}) error {
	_, err := c.pool.Exec(context, sql, args...)
	return err
}

// Exec run a query against the database
func (c *DBClient) Exec(context context.Context, sql string) error {
	_, err := c.pool.Exec(context, sql)
	return err
}

// Query run a query against the database
func (c *DBClient) Query(context context.Context, sql string) (pgx.Rows, error) {
	rows, err := c.pool.Query(context, sql)
	if err != nil {
		return rows, err
	}
	return rows, nil
}
