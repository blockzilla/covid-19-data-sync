package database

import (
	"context"
	"encoding/csv"
	"io"
	"io/ioutil"
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/blockzilla/covid-19-data-sync/models"
	"gitlab.com/blockzilla/covid-19-data-sync/pkg/utils"
)

// Countries interface methods for manipulating countries tables
type Countries interface {
	CreateCountries(context.Context, *models.Country) error
	ProcessCountries(context.Context, []byte) error
}

// CountriesSQL interface methods for manipulating data tables
type CountriesSQL struct {
	db     *DBClient
	logger *logrus.Logger
}

// CreateCountries create data in the database
func (s *CountriesSQL) CreateCountries(context context.Context, country *models.Country) error {

	logrus.Infof(`%s, %s, %d, %d, %d, %d, %d`,
		country.Date,
		country.Country,
		country.TotalConfirmed,
		country.TotalDead,
		country.TotalRecovered,
		country.TotalActive,
		country.TotalPercentage,
	)

	return s.db.ExecWithArgs(
		context,
		`INSERT INTO countries(
			date,
			country,
			total_confirmed,
			total_dead,
			total_recovered,
			total_active,
			total_percentage
		)
		values ($1, $2, $3, $4, $5, $6, $7)`,
		country.Date,
		country.Country,
		country.TotalConfirmed,
		country.TotalDead,
		country.TotalRecovered,
		country.TotalActive,
		country.TotalPercentage,
	)
}

// ProcessCountries work country list and add to database
func (s *CountriesSQL) ProcessCountries(ctx context.Context, d []byte) error {
	if d != nil {
		fileName := "geo-countries-data.csv"

		err := ioutil.WriteFile(fileName, d, 0644)
		if err != nil {
			return err
		}

		file, err := os.Open(fileName)
		if err != nil {
			logrus.Fatalln("Couldn't open the csv file", err)
			return err
		}

		// Parse the file
		r := csv.NewReader(file)
		logrus.Info("Writing data to database")

		var firstRow = true
		// Iterate through the record rows
		for {
			// process each record from csv
			record, err := r.Read()
			if err == io.EOF {
				break
			}
			if err != nil {
				return err
			}
			if firstRow == true {
				firstRow = false
			} else {
				err = s.CreateCountries(
					context.Background(),
					&models.Country{
						Date:            record[0],
						Country:         record[1],
						TotalConfirmed:  utils.StringToInt64(record[2]),
						TotalDead:       utils.StringToInt64(record[3]),
						TotalRecovered:  utils.StringToInt64(record[4]),
						TotalActive:     utils.StringToInt64(record[5]),
						TotalPercentage: 0,
					},
				)
				if err != nil {
					logrus.Error(err.Error())
					return err
				}
			}
		}
	}
	logrus.Info("Database update complete")
	return nil
}
