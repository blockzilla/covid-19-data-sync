package utils

import (
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

// TODO: move this to hadoop jobs, i.e. pig, pyspark.... ect

// SanatiseDate process and formate a varity of date strings
func SanatiseDate(dateStr string) time.Time {

	// date := "2018-10-24T18:50:23.541Z"
	var dateFinal = time.Now()
	// 2/1/2020 1:52
	// 1/25/20 17:00
	if strings.Contains(dateStr, "/") {

		// logrus.Print("dateStr before processing: ", dateStr)

		// massage the data
		if len(dateStr) == 13 {

			b := Before(dateStr, " ")
			a := After(dateStr, " ")

			// handle 2/1/2020 17:00
			if b == "2/1/2020" {
				dateStr = "0" + dateStr
				dateStr = "02/01/2020" + " " + a
			} else {
				// handle 1/22/20 17:00
				dateStr = "0" + b + "20" + " " + a
			}
		} else if len(dateStr) == 15 {
			// handle 1/22/2020 17:00
			dateStr = "0" + dateStr
		} else if len(dateStr) == 14 {
			// these dates are totall jumbled
			b := Before(dateStr, " ")
			a := After(dateStr, " ")

			// thankfully its only feb ths first & second after this the data is clean
			if b == "2/1/2020" {
				b = "02/01/2020"
			} else if b == "1/31/2020" {
				b = "01/31/2020"
			}
			// fix time
			if len(a) == 4 {
				a = "0" + a
			}
			dateStr = b + " " + a
		}
		// logrus.Print("dateStr: " + dateStr)

		dataTmp, _ := time.Parse("01/02/2006 15:04", dateStr)
		dataTmp, _ = time.Parse("2006-01-02T15:04:05", dataTmp.Format("2006-01-02T15:04:05"))
		dateFinal = dataTmp
	} else if strings.Contains(dateStr, " ") && len(dateStr) == 19 {
		dateFinal, _ = time.Parse("2006-01-02 15:04:05", dateStr)
	} else {
		// https://programming.guide/go/format-parse-string-time-date-example.html
		dateFinal, _ = time.Parse("2006-01-02T15:04:05", dateStr)
	}

	logrus.Infof("Processing records for: " + dateFinal.String())
	logrus.Infof("-----------------")
	return dateFinal
}
