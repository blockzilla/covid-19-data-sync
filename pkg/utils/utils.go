package utils

import (
	"strconv"
	"strings"
)

// CheckData check we are not the fist row of the csv
func CheckData(str string) bool {

	if str == "Confirmed" {
		return false
	}
	return true
}

// CheckFIPS check we are not the fist row of the csv
func CheckFIPS(str string) bool {

	if str == "FIPS" {
		return false
	}
	return true
}

// NilOrString string to string
func NilOrString(str string) string {

	if len(str) == 0 {
		return ""
	}
	return str
}

// StringToInt64 convert string to int64
func StringToInt64(str string) int64 {
	number, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		return 0
	}
	return number
}

// StringToFloat64 convert string to float64
func StringToFloat64(str string) float64 {
	number, err := strconv.ParseFloat(str, 8)

	if err != nil {
		return 0
	}
	return number
}

// StringToInt convert string to int
func StringToInt(str string) int {
	number, err := strconv.Atoi(str)
	if err == nil {
		return 0
	}

	return number
}

// FloatToString convert float to string
func FloatToString(num float64) string {
	// to convert a float number to a string
	return strconv.FormatFloat(num, 'f', 6, 64)
}

// Int64ToString convert int64 to string
func Int64ToString(num int64) string {
	return strconv.FormatInt(num, 10)
}

// After split string after char
func After(value string, a string) string {
	pos := strings.LastIndex(value, a)
	if pos == -1 {
		return ""
	}
	adjustedPos := pos + len(a)
	if adjustedPos >= len(value) {
		return ""
	}
	return value[adjustedPos:len(value)]
}

// Before split string before char
func Before(value string, a string) string {
	// Get substring before a string.
	pos := strings.Index(value, a)
	if pos == -1 {
		return ""
	}
	return value[0:pos]
}
