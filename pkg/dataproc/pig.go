package dataproc

import (
	"context"
	"fmt"
	"io/ioutil"
	"time"

	"github.com/sirupsen/logrus"

	dataproc "cloud.google.com/go/dataproc/apiv1"
	"cloud.google.com/go/storage"
	cmd "gitlab.com/blockzilla/covid-19-data-sync/cmd"
	"google.golang.org/api/option"
	dataprocpb "google.golang.org/genproto/googleapis/cloud/dataproc/v1"
)

// PigJob execute a job calling the Pig api
func PigJob(cfg *cmd.Config, endpoint string, clusterClient *dataproc.ClusterControllerClient) {
	ctx := context.Background()

	// Create the job client.
	jobClient, err := dataproc.NewJobControllerClient(ctx, option.WithEndpoint(endpoint))

	// file to run from bucket
	jobFile := fmt.Sprintf("gs://%s/%s", cfg.BucketName, cfg.PigJobs.Job)

	// Create the job config.
	submitJobReq := &dataprocpb.SubmitJobRequest{
		ProjectId: cfg.ProjectID,
		Region:    cfg.Region,
		Job: &dataprocpb.Job{
			Placement: &dataprocpb.JobPlacement{
				ClusterName: cfg.ClusterName,
			},
			TypeJob: &dataprocpb.Job_PigJob{
				PigJob: &dataprocpb.PigJob{
					Queries: &dataprocpb.PigJob_QueryFileUri{
						QueryFileUri: jobFile,
					},
				},
			},
		},
	}

	submitJobResp, err := jobClient.SubmitJob(ctx, submitJobReq)
	if err != nil {
		logrus.Errorf("error submitting job: %v\n", err)
		return
	}

	id := submitJobResp.Reference.JobId

	logrus.Infof("Submitted job %q\n", id)

	// These states all signify that a job has terminated, successfully or not.
	terminalStates := map[dataprocpb.JobStatus_State]bool{
		dataprocpb.JobStatus_ERROR:     true,
		dataprocpb.JobStatus_CANCELLED: true,
		dataprocpb.JobStatus_DONE:      true,
	}

	// We can create a timeout such that the job gets cancelled if not in a terminal state after a certain amount of time.
	timeout := 5 * time.Minute
	start := time.Now()

	var state dataprocpb.JobStatus_State
	for {
		if time.Since(start) > timeout {
			cancelReq := &dataprocpb.CancelJobRequest{
				ProjectId: cfg.ProjectID,
				Region:    cfg.Region,
				JobId:     id,
			}

			if _, err := jobClient.CancelJob(ctx, cancelReq); err != nil {
				logrus.Errorf("error cancelling job: %v\n", err)
			}
			logrus.Infof("job %q timed out after %d minutes\n", id, int64(timeout.Minutes()))
			return
		}

		getJobReq := &dataprocpb.GetJobRequest{
			ProjectId: cfg.ProjectID,
			Region:    cfg.Region,
			JobId:     id,
		}
		getJobResp, err := jobClient.GetJob(ctx, getJobReq)
		if err != nil {
			logrus.Errorf("error getting job %q with error: %v\n", id, err)
			return
		}
		state = getJobResp.Status.State
		if terminalStates[state] {
			break
		}

		// Sleep as to not excessively poll the API.
		time.Sleep(1 * time.Second)
	}

	// Cloud Dataproc job outget gets saved to a GCS bucket allocated to it.
	getCReq := &dataprocpb.GetClusterRequest{
		ProjectId:   cfg.ProjectID,
		Region:      cfg.Region,
		ClusterName: cfg.ClusterName,
	}

	resp, err := clusterClient.GetCluster(ctx, getCReq)
	if err != nil {
		logrus.Errorf("error getting cluster %q: %v\n", cfg.ClusterName, err)
		return
	}

	storageClient, err := storage.NewClient(ctx)
	if err != nil {
		logrus.Errorf("error creating storage client: %v\n", err)
		return
	}

	obj := fmt.Sprintf("google-cloud-dataproc-metainfo/%s/jobs/%s/driveroutput.000000000", resp.ClusterUuid, id)
	reader, err := storageClient.Bucket(resp.Config.ConfigBucket).Object(obj).NewReader(ctx)
	if err != nil {
		logrus.Errorf("error reading job output: %v\n", err)
		return
	}

	defer reader.Close()

	body, err := ioutil.ReadAll(reader)
	if err != nil {
		logrus.Errorf("could not read output from Dataproc Job %q\n", id)
		return
	}

	logrus.Infof("job %q finished with state %s:\n%s\n", id, state, body)
}
