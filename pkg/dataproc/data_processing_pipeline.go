package dataproc

import (
	"context"
	"fmt"

	dataproc "cloud.google.com/go/dataproc/apiv1"
	"github.com/sirupsen/logrus"
	cmd "gitlab.com/blockzilla/covid-19-data-sync/cmd"
	"google.golang.org/api/option"
	dataprocpb "google.golang.org/genproto/googleapis/cloud/dataproc/v1"
)

// DataProcessPipeline create a cluster, run data processing jobs and delete
func DataProcessPipeline(cfg *cmd.Config) {
	endpoint := fmt.Sprintf("%s-dataproc.googleapis.com:443", cfg.Region)

	logrus.Infof("Creating dataproc cluster")

	ctx := context.Background()

	// Create the cluster client.
	clusterClient, err := dataproc.NewClusterControllerClient(ctx, option.WithEndpoint(endpoint))
	if err != nil {
		logrus.Fatalf("error creating the cluster client: %s\n", err)
	}

	// Create the cluster config.
	createReq := &dataprocpb.CreateClusterRequest{
		ProjectId: cfg.ProjectID,
		Region:    cfg.Region,
		Cluster: &dataprocpb.Cluster{
			ProjectId:   cfg.ProjectID,
			ClusterName: cfg.ClusterName,
			Config: &dataprocpb.ClusterConfig{
				MasterConfig: &dataprocpb.InstanceGroupConfig{
					NumInstances:   1,
					MachineTypeUri: "n1-standard-1",
				},
				WorkerConfig: &dataprocpb.InstanceGroupConfig{
					NumInstances:   2,
					MachineTypeUri: "n1-standard-1",
				},
			},
		},
	}

	// Create the cluster.
	createOp, err := clusterClient.CreateCluster(ctx, createReq)
	if err != nil {
		logrus.Fatalf("error submitting the cluster creation request: %v\n", err)
	}

	createResp, err := createOp.Wait(ctx)
	if err != nil {
		logrus.Fatalf("error creating the cluster: %v\n", err)
	}

	// Defer cluster deletion.
	defer func() {
		dReq := &dataprocpb.DeleteClusterRequest{
			ProjectId:   cfg.ProjectID,
			Region:      cfg.Region,
			ClusterName: cfg.ClusterName,
		}
		deleteOp, err := clusterClient.DeleteCluster(ctx, dReq)
		deleteOp.Wait(ctx)
		if err != nil {
			logrus.Printf("error deleting cluster %q: %v\n", cfg.ClusterName, err)
			return
		}
		logrus.Printf("Cluster %q successfully deleted\n", cfg.ClusterName)
	}()

	// Output a success message.
	logrus.Printf("Cluster created successfully: %q\n", createResp.ClusterName)

	// run the data processing pipeline
	PigJob(cfg, endpoint, clusterClient)
	PysparkJob(cfg, endpoint, cfg.PysparkJobs.Job, clusterClient)
}
