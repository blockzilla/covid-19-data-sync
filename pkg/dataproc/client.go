package dataproc

import (
	"context"
	"fmt"

	"github.com/sirupsen/logrus"

	dataproc "cloud.google.com/go/dataproc/apiv1"
	"google.golang.org/api/option"
	dataprocpb "google.golang.org/genproto/googleapis/cloud/dataproc/v1"

	cmd "gitlab.com/blockzilla/covid-19-data-sync/cmd"
)

// ClusterClient create a dataproc cluster
func ClusterClient(cfg *cmd.Config) *dataproc.ClusterControllerClient {
	endpoint := fmt.Sprintf("%s-dataproc.googleapis.com:443", cfg.Region)

	logrus.Infof("Creating dataproc cluster")
	// mc.LogMessage("Creating dataproc cluster", cfg.AmqpConfig.LogQueue)

	ctx := context.Background()

	// Create the cluster client.
	clusterClient, err := dataproc.NewClusterControllerClient(ctx, option.WithEndpoint(endpoint))
	if err != nil {
		logrus.Fatalf("error creating the cluster client: %s\n", err)
		// mc.LogMessage(logrus.Sprintf("error creating the cluster client: %s\n", err), cfg.AmqpConfig.LogQueue)
	}

	// Create the cluster config.
	createReq := &dataprocpb.CreateClusterRequest{
		ProjectId: cfg.ProjectID,
		Region:    cfg.Region,
		Cluster: &dataprocpb.Cluster{
			ProjectId:   cfg.ProjectID,
			ClusterName: cfg.ClusterName,
			Config: &dataprocpb.ClusterConfig{
				MasterConfig: &dataprocpb.InstanceGroupConfig{
					NumInstances:   1,
					MachineTypeUri: "n1-standard-1",
				},
				WorkerConfig: &dataprocpb.InstanceGroupConfig{
					NumInstances:   2,
					MachineTypeUri: "n1-standard-1",
				},
			},
		},
	}

	// Create the cluster.
	createOp, err := clusterClient.CreateCluster(ctx, createReq)
	if err != nil {
		logrus.Fatalf("error submitting the cluster creation request: %v\n", err)
		// mc.LogMessage(logrus.Sprintf("error submitting the cluster creation request:%s\n", err), cfg.AmqpConfig.LogQueue)
	}

	createResp, err := createOp.Wait(ctx)
	if err != nil {
		logrus.Fatalf("error creating the cluster: %v\n", err)
		// mc.LogMessage(logrus.Sprintf("error creating the cluster: %s\n", err), cfg.AmqpConfig.LogQueue)
	}

	// Defer cluster deletion.
	defer func() {
		dReq := &dataprocpb.DeleteClusterRequest{
			ProjectId:   cfg.ProjectID,
			Region:      cfg.Region,
			ClusterName: cfg.ClusterName,
		}
		deleteOp, err := clusterClient.DeleteCluster(ctx, dReq)
		deleteOp.Wait(ctx)
		if err != nil {
			logrus.Printf("error deleting cluster %q: %v\n", cfg.ClusterName, err)
			return
		}
		logrus.Printf("Cluster %q successfully deleted\n", cfg.ClusterName)
		// mc.LogMessage("Cluster successfully deleted", cfg.AmqpConfig.LogQueue)
	}()

	// Output a success message.
	logrus.Printf("Cluster created successfully: %q\n", createResp.ClusterName)
	// mc.LogMessage("Cluster created successfully", cfg.AmqpConfig.LogQueue)
	return clusterClient
}
